HTML : Principes de base

Q18 : L’écriture <body> signifie : Début du corps de la page Web et Fin du corps de la page Web. Le mot body vient de l’anglais qui veut dire corps

Q19 : HTML utilise des Balises fixes définis par le langage et Balises définies par l’utilisateur

Q20 : La majorité des balises HTML ont la structure : <BALISE> ... </BALISE>

Q21 : Un document HTML contient une balise racine appelée

HTML elle signifie que le document en question est un document HTML

Q22 : Quelle est la bonne façon de terminer un document HTML ?
</HTML>

Q23 : Une paire de balises HTML doit être utilisé dans vos pages web, une au début et l'autre sur la dernière ligne. De quelle paire s'agit-il ?
<HTML> et </HTML>

Q24 : Indiquez une balise obligatoire pour fabriquer une page web
<HTML>

Q25 : Où sont normalement placées les balises <HTML> et </HTML> dans un document HTML ?
Au début et à la fin du document car elle signifie le début et la fin 

Q26 : Dans quelle paire de balises HTML trouve-t-on la majorité des autres balises qui composent un document HTML?
<HEAD> et </HEAD> car c’est l’entête 

Q27 : Laquelle des propriétés suivantes définit la taille de la police du texte ?
Font-size
