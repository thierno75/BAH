package com.example.nono_appli

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity_hotel_description.setOnClickListener{
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            startActivity(intent)
        }
    }
}