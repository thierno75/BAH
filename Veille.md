# Veille technologique

## Sujet : La 5G

**14 Octobre 2020**

* Les craintes sur la 5G au yeux du peuples :

        https://www.usine-digitale.fr/article/pour-rassurer-sur-la-5g-le-gouvernement-renforce-les-controles-d-exposition-aux-ondes.N1015754

* Les Fabricants de smartphone se préparent pour le combat de la 5G :

        https://www.latribune.fr/technos-medias/telecoms/5g-l-iphone-12-un-smartphone-tres-attendu-par-les-operateurs-telecoms-859683.html
        https://www.presse-citron.net/lacces-a-la-5g-devient-encore-plus-abordable-chez-samsung/
        https://www.frandroid.com/bons-plans/bons-plans-smartphone/782887_le-oneplus-8-5g-passe-lui-aussi-a-moins-de-500-e-pour-le-prime-day

*

