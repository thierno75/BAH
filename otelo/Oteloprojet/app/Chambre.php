<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Chambre extends Model
{
    protected $table = "chambre"; 
    protected $fillable = [
       'nbCouchage' ,'porte','etage','idCategorie','baignoire','prixBase'
    ];
    
}
