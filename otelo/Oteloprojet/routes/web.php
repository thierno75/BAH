<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/accueil', function () {
    //return view('accueil');
//});

Route::get('/accueil','PremierController@home' );
Route::get('/chambre','ChambreController@store' );
Route::get('/newreservation','ReservationController@create' )->middleware('auth');
Route::post('/storereservation','ReservationController@store' )->name('reservation.store');
Route::get('/failure',function () {
    return view('failure');
})->name('failure');


