*
# Veille technologique

## Sujet : La 5G

**12 Octobre 2020**


* Le Gouvernement veut superviser le controle de la 5G sur les Smartphones via le DAS(Débit d'absorption spécifique)

Ceci est un article provenant de BFMTV publié le 12 Octobre 2020. Cet article nous expliquent les craintes au niveaux de la 5G avec les eventuelles problemes de santé qu'elles pourraient engrangés via une sur-exposition des ondes électromagnectiques.

Pour répondre à ce désacord des "Anti 5G" et rassuré le peuple français Gouvernement dévoila un plan de controle par le biais du Cédric O, le secrétaire d'Etat au numérique qui consiste à doublé les controles auprés de l'AFNR(l’Agence nationale des Fréquences).

Pour réalisé ses controles l'AFNR mesure le DAS à savoir le (débit d'absorption spécifique) qui evalue l'energie qu'absorbe le corps humain lors d'une utilisation d'un appareil électronique en  prélevant les Smartphones commercialisés et font différents test rendus public (70 appareils en 2019 reporté à 140 en 2021).






 https://www.bfmtv.com/tech/5g-le-gouvernement-veut-rassurer-en-multipliant-les-controles-d-exposition-aux-ondes_AN-202010120276.html
                
               





**13 Octobre 2020**

* Les craintes sur la 5G au yeux du peuples :

        https://www.usine-digitale.fr/article/pour-rassurer-sur-la-5g-le-gouvernement-renforce-les-controles-d-exposition-aux-ondes.N1015754


* Les Fabricants de smartphone se préparent pour le combat de la 5G :

        https://www.latribune.fr/technos-medias/telecoms/5g-l-iphone-12-un-smartphone-tres-attendu-par-les-operateurs-telecoms-859683.html
        https://www.presse-citron.net/lacces-a-la-5g-devient-encore-plus-abordable-chez-samsung/
        https://www.frandroid.com/bons-plans/bons-plans-smartphone/782887_le-oneplus-8-5g-passe-lui-aussi-a-moins-de-500-e-pour-le-prime-day


* Le Paradoxe sur la 5G  :

                         https://www.la-croix.com/Sciences-et-ethique/Faut-avoir-peur-5G-2020-10-13-1201119171