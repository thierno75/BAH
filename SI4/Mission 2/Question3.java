//BAH Thierno-Abdoul
//21/11/19
//Mission 2 Question2.java
/*Objectif : Compléter le programme de la Question 1. On demandera à l’utilisateur d’indiquer si le
produit saisi est de première nécessité (TVA à 5%) ou pas (TVA à 20%). Ne pas oublier d’afficher un
message explicite à l’utilisateur pour qu’il sache quoi saisir.
*/
import java.util.Scanner;
class Question3{
	public static void main (String[] args){
		
		Scanner sc=new Scanner(System.in);//On prepare l'outil scanner
		
		System.out.println(" Code secret  ? " );
		String codesecret=sc.nextLine();
		System.out.println(codesecret);
		if(codesecret.equals("Padawan")) {
		
			double tauxdeTVA=0.20; //On initialise tauxdeTVA comme étant un réel qui vaut 0.25
			System.out.println( " Veuillez saisir le nom de votre produit " );
			String produit = sc.nextLine();
			System.out.println( " Quelle est le prix hors taxe " );
			double prixHT = sc.nextDouble();
			System.out.println( " Combien de quantite achetee ");
			int quantite = sc.nextInt();
			System.out.println( " est-ce un produit de premiere necessite (true / false) ? ");
			boolean premiereN = sc.nextBoolean();
			if (premiereN) {
				tauxdeTVA= 0.05;
			}
			if (quantite>100){//variable si la quantité est supérieur vous possédez une remise 
				System.out.println(" Vous avez droit à une remise de 10% "); // affiche le message de remise 10%
				
				double prixTTC;//on initialise prixTTC comme étant un réele
				prixTTC = prixHT*(1+tauxdeTVA)*quantite;//Calculer prixTTC
				double Prixreduc= prixTTC*0.90; 
				System.out.println("Le prix TTC de votre panier de " +produit +" est : " + Prixreduc  +" euros ");//Affiche le message + le nom du produit+le prix TTC
			}
			else {// else veut dire sinon
				
				double prixTTC;//on initialise prixTTC comme étant un réele
				prixTTC = prixHT*(tauxdeTVA+1)*quantite;
				System.out.println(" Vous ne beneficiez pas de la reduction ");
				System.out.println( " Le prix TTC de votre panier de " +produit +" est : " +prixTTC +" euros ");											
			}
		} else {
			System.out.println( " au revoir " );
		}
	}
}