//BAH Thierno-Abdoul
//21/11/19
//Mission 2 Question2.java
/*Objectif : Compléter le programme de la Question 1. On demandera à l’utilisateur d’indiquer si le
produit saisi est de première nécessité (TVA à 5%) ou pas (TVA à 20%). Ne pas oublier d’afficher un
message explicite à l’utilisateur pour qu’il sache quoi saisir.
*/
import java.util.Scanner;//on prepare l'outil Scanner
class Question6{
	public static void main (String[] args){
		Scanner sc=new Scanner(System.in);//On prepare l'outil scanner
		double tauxdeTVA=0.20; //On initialise tauxdeTVA comme étant un réel qui vaut 0.25
		System.out.println( " Veuillez saisir le nom de votre produit " );//affiche message
		String produit = sc.nextLine();//Initialise produit comme étant string
		System.out.println( " Quelle est le prix hors taxe " );//afficheer message
		double prixHT = sc.nextDouble();//initialise prixHT
		System.out.println( " Combien de quantite achetee ");//affiche message
		int quantite = sc.nextInt();//initialise quantite
		System.out.println( " est-ce un produit de premiere necessite (true / false) ? ");//affiche message
		boolean premiereN = sc.nextBoolean();//boolean vrai ou faux définit premiereN comme étant boolean
		if (premiereN) {//si premiereN est true alors
			tauxdeTVA= 0.05;//tauxdeTVA=0.05
		}
		if (quantite>100){//variable si la quantité est supérieur vous possédez une remise 
			System.out.println(" Vous avez droit à une remise de 10% "); // affiche le message de remise 10%
			
			double prixTTC;//on initialise prixTTC comme étant un réele
			prixTTC = prixHT*(1+tauxdeTVA)*quantite;//Calculer prixTTC
			double Prixreduc= prixTTC*0.90; //Prixreduc c'est le prixTTC*0.90 une reduction de 10%
			System.out.println("Le prix TTC de votre panier de " +produit +" est : " + Prixreduc  +" euros ");//Affichez le message + le nom du produit+le prix TTC
		}
		else {// else veut dire sinon. si ke produit n'est pas un produit de premiere necessite alors vous beneficiez de la reduction et votre tva est de 20%
			
			double prixTTC;//on initialise prixTTC comme étant un réele
			prixTTC = prixHT*(tauxdeTVA+1)*quantite;//
			System.out.println(" Vous ne beneficiez pas de la reduction  ");//affiche message
			System.out.println( " Le prix TTC de votre panier de " +produit +" est : " +prixTTC +" euros ");											
		}
	}
}