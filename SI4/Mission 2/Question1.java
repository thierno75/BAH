//BAH Thierno-Abdoul 
//18/11/19
//Mission 2 Question1.java
/*Objectif : Compléter le programme de la Mission 1 en accordant une remise de 10 % sur le prix TTC
pour tout achat de plus de 100 pièces. Ecrire le programme calculant le prix TTC en fonction de la
quantité, du prix HT et du taux de TVA.
*/
import java.util.Scanner;
class Question1{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);//On prepare l'outil Scanner
		System.out.println(" Veuillez saisir le nom de votre produit " );//on affiche le message de saisir le nom du produit
		String produit = sc.nextLine();//on demande a l'utilisateur de saisir une valeur qui sera une chaine de caractere
		System.out.println(" Quelle est le prix Hors taxe ? ");// on affiche le message qui demande le prix hors taxe
		double prixHT = sc.nextDouble();//on demande à l'utilisateur de saisir une valeur qui sera un réel
		System.out.println(" Combien de quantite achetee ? ");// on affiche le message qui demande la quantité achatée
		int quantite = sc.nextInt();//on recuper la 3eme valeur saisi
		if (quantite>100){//variable si la quantité est supérieur vous possédez une remise 
			System.out.println(" Vous avez droit à une remise de 10% "); // affiche le message de remise 10%
			double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réel qui vaut 0.25
			double prixTTC;//on initialise prixTTC comme étant un réele
			prixTTC = prixHT*(1+tauxdeTVA)*quantite;//Calculer prixTTC
			double Prixreduc= prixTTC*0.90; 
			System.out.println("Le prix TTC de votre panier de " +produit +" est : " + Prixreduc  +" euros ");//Affiche le message + le nom du produit+le prix TTC
		}
		else {// else veut dire sinon
			double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réele qui vaut 0.25
			double prixTTC;//on initialise prixTTC comme étant un réele
			prixTTC = prixHT*(tauxdeTVA+1)*quantite;
			System.out.println(" Vous ne beneficiez pas de la reduction ");
			System.out.println( " Le prix TTC de votre panier de " +produit +" est : " +prixTTC +" euros ");											
		}
	}
}	