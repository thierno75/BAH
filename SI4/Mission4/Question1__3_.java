//fichier: Question2.java
//Bah Thierno 22/11/19
/*affiche des articles et leur prix à la suite */

class Question1 {
    public static void main(String[] args) {
        String[] noms = { "pain", "BMW", "Ford", "Mazda", "audi" };
        double[] prixht = { 15.5, 25, 99, 47, 75 };
        for (int i = 0; i < 5; i++) {
            System.out.println("Le prix ht du produit " + noms[i] + " est " + prixht[i] + " euros");
        }
    }
}
