
//fichier: Question2.java
//Bah Thierno 22/11/19
/*affiche des articles et leur prix à la suite */
import java.util.Scanner;

class Question2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] noms = { "pain", "BMW", "Ford", "Mazda", "audi" };
        double[] prixht = new double[5];
        for (int i = 0; i < 5; i++) {
            System.out.println("quel est Le prix ht du produit " + noms[i] + " ?");
            double prix = sc.nextDouble();// on demande à l'utilisateur de saisir une valeur qui sera un réel
            prixht[i] = prix;

        }
        for (int i = 0; i < 5; i++) {
            System.out.println("Le prix ht du produit " + noms[i] + " est " + prixht[i] + " euros");

        }
    }
}
