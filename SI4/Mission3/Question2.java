//BAH Thierno-Abdoul 
//18/11/19
//Mission 3 Question2.java
/*Objectif : Compléter le programme de la Mission 2 (Question1) pour permettre à l’utilisateur de
saisir le nombre de à produits à traiter « n ». On proposera ensuite le calcul du prixTTC d’un nouveau
produit « n » fois. Avant de quitter, on n’oublie pas de saluer l’utilisateur !
*/

import java.util.Scanner;
class Question2{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);//On prepare l'outil Scanner
		System.out.println("saisir le nombre de produits à traiter");
		int nvxpdt
		nvxpdt=sc.nextInt();
		System.out.println("le nombre de produit à traiter est: " + nvxpdt);
		for(int i=0; i<=15; i++ ){
			Scanner sc2=new Scanner(System.in);
			System.out.println(" Veuillez saisir le nom de votre produit " );//on affiche le message de saisir le nom du produit
			String produit = sc2.nextLine();//on demande a l'utilisateur de saisir une valeur qui sera une chaine de caractere
			System.out.println(" Quelle est le prix Hors taxe ? ");// on affiche le message qui demande le prix hors taxe
			double prixHT = sc2.nextDouble();//on demande à l'utilisateur de saisir une valeur qui sera un réel
			System.out.println(" Combien de quantite achetee ? ");// on affiche le message qui demande la quantité achatée
			int quantite = sc2.nextInt();//on recuper la 3eme valeur saisi
			if (quantite>100){
				System.out.println(" Vous avez droit à une remise de 10% "); // affiche le message de remise 10%
				double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réel qui vaut 0.25
				double prixTTC;//on initialise prixTTC comme étant un réele
				prixTTC = prixHT*(1+tauxdeTVA)*quantite;//Calculer prixTTC
				double Prixreduc= prixTTC*0.90; 
				System.out.println("Le prix TTC de votre panier de " +produit +" est : " + Prixreduc  +" euros ");//Affiche le message + le nom du produit+le prix TTC
			}
			else{
				double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réele qui vaut 0.25
				double prixTTC;//on initialise prixTTC comme étant un réele
				prixTTC = prixHT*(tauxdeTVA+1)*quantite;
				System.out.println(" Vous ne beneficiez pas de la reduction ");
				System.out.println( " Le prix TTC de votre panier de " +produit +" est : " +prixTTC +" euros ");
			}
		}
		System.out.println(" Merci beaucoup au revoir !!!");
	}
}
			

