//BAH Thierno-Abdoul 
//18/11/19
/*Objectif : Compléter le programme de la Mission 2 (Question1) pour permettre à l’utilisateur de
recommencer (ou pas !) le calcul du prixTTC d’un nouveau produit (proposer un choix tel que 1
permet de continuer et 0 permet de quitter). Avant de quitter, on salue l’utilisateur !
*/

import java.util.Scanner;
class Question15{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);//On prepare l'outil Scanner
		int nvxpdt=0;
		System.out.println("veuillez taper 1 pour continuer ou 0 pour quitter");
		nvxpdt=sc.nextInt();
		while(nvxpdt==1){
			Scanner sc1=new Scanner(System.in);
			System.out.println(" Veuillez saisir le nom de votre produit " );//on affiche le message de saisir le nom du produit
			String produit = sc1.nextLine();//on demande a l'utilisateur de saisir une valeur qui sera une chaine de caractere
			System.out.println(" Quelle est le prix Hors taxe ? ");// on affiche le message qui demande le prix hors taxe
			double prixHT = sc1.nextDouble();//on demande à l'utilisateur de saisir une valeur qui sera un réel
			System.out.println(" Combien de quantite achetee ? ");// on affiche le message qui demande la quantité achatée
			int quantite = sc1.nextInt();//on recuper la 3eme valeur saisi
				if (quantite>100){
					System.out.println(" Vous avez droit à une remise de 10% "); // affiche le message de remise 10%
					double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réel qui vaut 0.25
					double prixTTC;//on initialise prixTTC comme étant un réele
					prixTTC = prixHT*(1+tauxdeTVA)*quantite;//Calculer prixTTC
					double Prixreduc= prixTTC*0.90;
					System.out.println("Le prix TTC de votre panier de " +produit +" est : " + Prixreduc  +" euros ");//Affiche le message + le nom du produit+le prix TTC
				}
				else {
					double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réele qui vaut 0.25
					double prixTTC;//on initialise prixTTC comme étant un réele
					prixTTC = prixHT*(tauxdeTVA+1)*quantite;
					System.out.println(" Vous ne beneficiez pas de la reduction ");
					System.out.println( " Le prix TTC de votre panier de " +produit +" est : " +prixTTC +" euros ");
				}
		System.out.println("Merci beaucoup au revoir");
		}
		
	}
}