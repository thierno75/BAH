//BAH Thierno-Abdoul 
//18/11/19
//Mission 1 Question1.java
/*
Objectif : Identifier les différentes variables et donner leur type en complétant leur déclaration.
Afficher à l’écran chaque contenu de variable en exprimant leur type explicitement.
*/

class question1
{
	public static void main (String[] args){
		int age = 20; //on définit la variable du type entier initialisée à 20
		int i= 0; //on définit la variable i du type entier intialisée à 0
		i=1+1;//on declare i étant egale à 1+1
		int j=i;// on définit la variable j de type entier initialisée à i
		String k="coucou";//on définit la variable k du type chaine de caractere à coucou
		String h=k+k;// on declare h étant egale à k+k 
		boolean majeur=(age>18);
		System.out.println("la variable age de type entier contient "+ age);// affiche le message 
		System.out.println("la variable i de type entier contient "+ i);// affiche le message  
		System.out.println("la variable j de type entier contient "+ j);// affiche le message  
		System.out.println("la variable k de type string contient "+ k);// affiche le message 
		System.out.println("la variable h de type string contient "+ h);// affiche le message 
		System.out.println("la variable majeur de type boolean contient "+ majeur);// affiche le message
	}
}	
