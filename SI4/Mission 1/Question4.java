//BAH Thierno-Abdoul 
//18/11/19
//Mission 1 Question4.java
/*
Objectif : Compléter le programme de la question 3. Le programme doit initialiser une nouvelle
donnée : le nom du produit concerné en demandant la valeur à l’utilisateur. Ensuite, le programme
affiche directement le prix TTC à payer de ce produit. N’oubliez pas d’afficher un message explicite à
l’utilisateur pour qu’il sache quoi saisir.
*/
import java.util.Scanner;
class Question4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);//on prépare l'outil scanner
		System.out.println(" Veuillez saisir le nom de votre produit " );//on affiche le message de saisir le nom du produit
		String produit = sc.nextLine();//on demande a l'utilisateur de saisir une valeur qui sera une chaine de caractere
		System.out.println(" Quelle est le prix Hors taxe ? ");// on affiche le message qui demande le prix hors taxe
		double prixHT = sc.nextInt();//on demande à l'utilisateur de saisir une valeur qui sera un réele
		System.out.println(" Combien de quantite achetee ? ");;// on affiche le message qui demande la quantité achatée
		int quantite = sc.nextInt();//on demande à l'utilisateur de saisir la quantite qui sera un entier
		double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réele qui vaut 0.25
		double prixTTC;//on initialise prixTTC comme étant un réele
		prixTTC = prixHT*(tauxdeTVA+1)*quantite;//Calculer prixTTC
		System.out.println(" Le prixTTC du produit " +produit + " est de : " + prixTTC);//Affiche le nom du produit et le prix TTC
	}
}

/*
ALGO
algorithme Question4
prixHT,prixTTC,tauxdeTVA:variables type reeles
quantite:variable type entier
produit:type chaine de caractere
	debut
	afficher " Veuillez saisir le nom de votre produit "
	saisir produit
	afficher " Quelle est le prix Hors taxe ? "
	saisir prix Hors taxe
	afficher " Combien de quantite achetee "
	saisir quantite
	tauxdeTVA=0.25
	prixTTC = prixHT*(tauxdeTVA+1)*quantite
	afficher " Le prixTTC du produit " +produit + " est de : " + prixTTC
fin 