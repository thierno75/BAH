//BAH Thierno-Abdoul 
//18/11/19
//Mission 1 Question3.java


/*
Objectif : Compléter le programme de la question 2. Le programme doit commencer par initialiser la
quantité achetée et le prix HT de l’unité en demandant les valeurs à l’utilisateur. Ensuite, le
programme affiche directement le prix TTC à payer. N’oubliez pas d’afficher un message explicite à
l’utilisateur pour qu’il sache quoi saisir.
*/
import java.util.Scanner;//on récupere l'outil Scanner
class Question3{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);//on prépare l'outil scanner
		System.out.println(" Quelle est le prix Hors taxe ? ");// on affiche le message qui demande le prix hors taxe
		double prixHT = sc.nextInt();//on demande à l'utilisateur de saisir une valeur qui sera un réele
		System.out.println(" Combien de quantite achetee ? ");;// on affiche le message qui demande la quantité achatée
		int quantite = sc.nextInt();//on demande à l'utilisateur de saisir la quantité qui sera un entier
		double tauxdeTVA=0.25;//On initialise tauxdeTVA comme étant un réele qui vaut 0.25
		double prixTTC;//on initialise prixTTC comme étant un réele 
		prixTTC = prixHT*(tauxdeTVA+1)*quantite;//Calculer prixTTC 
		System.out.println(" Le prix TTC est : " + prixTTC);//Affiche le prixTTC
	}
}
	
/*
ALGO
algorithme Question3
variables tauxdeTVA,prixTTC,prixHT ce sont de types reeles
variables quantite est un entier
	debut
	afficher " Quelle est le prix Hors taxe ? "
	saisir prixHT
	afficher " Combien de quantite achetee ? " 
	saisir nombre de quantite achetee
	tauxdeTVA=0.25
	prixTTC = prixHT*(tauxdeTVA+1)*quantite
	affiche " le prix TTC est : " + prixTTC
fin
*/