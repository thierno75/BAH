//BAH Thierno-Abdoul 
//18/11/19
//Mission 1 Question2.java
/*
Objectif : Objectif : Écrire le programme qui affiche le prix total TTC en fonction du prix HT, du taux de tva et de
la quantité achetée (on prendra des valeurs au hasard)
*/

class question2{

	public static void main (String[] args){
		double prixHT, prixTTC, tauxDeTVA;//On initialise prixHT,prixTTC,tauxDeTVA comme étant un réele 
		int quantite;//0n initialise quantite comme étant un entier
		prixHT=75.50;
		tauxDeTVA=0.25;
		quantite=100;
		prixTTC = prixHT*(tauxDeTVA+1)*quantite;//cette formule sert à calculer le prixTTC
		System.out.println(" Le prix TTC de ce produit est " +prixTTC);//on affiche le prix TTC 
		
	}
} 
/* ALGO Question2
    debut
	  prixHT,prixTTC,tauxDeTVA variables réeles
	  quantite variable entier 
	  prixHT=75.50
	  tauxDeTVA=0.25
	  quantite=100
	  prixTTC = prixHT*(tauxDeTVA+1=*quantite)
	  affiche (" Le prix TTC de ce produit est " +prixTTC)
	Fin
*/
